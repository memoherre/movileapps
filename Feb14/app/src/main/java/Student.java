package mx.tec.feb_14;


public class Student {

    private String name;
    private float grade;
    private long studentID;

    public Student(String name, float grade, long id){
        this.name = name;
        this.grade = grade;
        this.studentID = id;
    }

    public String getName(){
        return this.name;
    }

    public float getGrade(){
        return this.grade;
    }

    public long getStudentID(){
        return this.studentID;
    }
}
