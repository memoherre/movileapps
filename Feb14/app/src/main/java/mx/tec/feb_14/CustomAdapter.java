package mx.tec.feb_14;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    private ArrayList<mx.tec.feb_14.Student> source;
    private Activity activity;

    public CustomAdapter(ArrayList<mx.tec.feb_14.Student> source, Activity activity){
        this.source = source;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.source.size();
    }

    @Override
    public Object getItem(int position) {
        return this.source.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.source.get(position).getStudentID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // The actual specific row
        if(convertView == null){
            //Inflate from xml to java object
            convertView = activity.getLayoutInflater().inflate(R.layout.row,null);
        }

        TextView name = convertView.findViewById(R.id.name);
        TextView grade = convertView.findViewById(R.id.grade);

        mx.tec.feb_14.Student theStudent = source.get(position);
        name.setText(theStudent.getName());
        grade.setText(theStudent.getGrade()+"");

        return convertView;
    }
}
