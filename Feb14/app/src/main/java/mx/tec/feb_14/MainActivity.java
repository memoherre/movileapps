package mx.tec.feb_14;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener ,AdapterView.OnItemSelectedListener{

    private ListView list;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.lista);
        spinner = findViewById(R.id.spinner);

        // widgets that deal with sets / groups
        // 3 parts

        // -data source
        // -Adapter :
        // -UI widget

        // dats source

        String [] data = {"Student 1", "Student 2", "Student 3"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                data
                );
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);


        ArrayList<mx.tec.feb_14.Student> students = new ArrayList<>();

        students.add(new mx.tec.feb_14.Student("Carlos",28,100));
        students.add(new mx.tec.feb_14.Student("Emmanuel",32,200));
        students.add(new mx.tec.feb_14.Student("Carlos",44,300));
        students.add(new mx.tec.feb_14.Student("Rosa",15,400));

        CustomAdapter customAdapter = new CustomAdapter(students,this);

        list.setAdapter(customAdapter);
        spinner.setAdapter(customAdapter);
        list.setOnItemClickListener(this);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this,"position: "+position + " id: "+id ,Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this,"position: "+position + " id: "+id ,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
