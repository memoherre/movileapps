package mx.itesm.shakeit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class RecyclerView_Config {
    private Context mContext;
    private CuponAdapter mCuponAdapter;

    public void setConfig(RecyclerView recyclerView, Context context, List<Cupon> cupones, List<String> keys){
        this.mContext = context;
        this.mCuponAdapter = new CuponAdapter(cupones, keys);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(mCuponAdapter);

    }

    class CuponItemView extends RecyclerView.ViewHolder {
        private TextView rest, prom;

        private String key;

        public CuponItemView(ViewGroup parent){
            super(LayoutInflater.from(mContext).inflate(R.layout.cupon_list_item, parent, false));

            rest = itemView.findViewById(R.id.restName);
            prom = itemView.findViewById(R.id.restProm);
        }

        public void bind(Cupon cupon, String key){
            rest.setText(cupon.getRestaurante());
            prom.setText(cupon.getInformacion());
            this.key = key;
        }
    }

    class CuponAdapter extends RecyclerView.Adapter<CuponItemView>{
        private List<Cupon> mCuponList;
        private List<String> mKeys;

        public CuponAdapter(List<Cupon> cupones, List<String> keys){
            this.mCuponList = cupones;
            this.mKeys = keys;
        }

        @NonNull
        @Override
        public CuponItemView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            return new CuponItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull CuponItemView cuponItemView, int i) {
            cuponItemView.bind(mCuponList.get(i), mKeys.get(i));
        }

        @Override
        public int getItemCount() {
            return mCuponList.size();
        }
    }
}
