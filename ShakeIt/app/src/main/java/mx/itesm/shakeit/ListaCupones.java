package mx.itesm.shakeit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListaCupones extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener{
    // public ListView lvLista;

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cupones);

        mRecyclerView = findViewById(R.id.recyclerView);

        new FirebaseDatabaseHelper().leerCupones(new FirebaseDatabaseHelper.DataStatus() {
            @Override
            public void DataIsLoaded(List<Cupon> cupones, List<String> keys) {
                new RecyclerView_Config().setConfig(mRecyclerView, ListaCupones.this, cupones, keys);
            }

            @Override
            public void DataIsInserted() {

            }

            @Override
            public void DataIsUpdated() {

            }

            @Override
            public void DataIsDeleted() {

            }
        });




       // lvLista = findViewById(R.id.lvLista);

        //data source
       // String[] data = {"Restaurante1", "Restaurante2", "Restaurante3"};

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);
/*
       ArrayList<Cupon> cupones = new ArrayList<>();
       cupones.add(new Cupon("El Restaurante de la Gina", "2x1", "1"));
       cupones.add(new Cupon("El Restaurante de la Frida", "50%", "2"));
       cupones.add(new Cupon("El otro restaurante", "10% en una compra mayor a $1000", "3"));


        CuponesAdapter customAdapter = new CuponesAdapter(cupones, this);

        lvLista.setAdapter(customAdapter);
        lvLista.setOnItemClickListener(this);
        */
    }

    public void changeToInformacionCupones(View v){
        Intent i = new Intent(this, InformacionCupones.class);
        startActivity(i);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, InformacionCupones.class);

        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
