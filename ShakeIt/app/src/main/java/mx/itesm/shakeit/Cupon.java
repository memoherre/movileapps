package mx.itesm.shakeit;

/**
 * Created by CLN-BRA on 26/02/2019.
 */

public class Cupon {
    private String restaurante, informacion, id;

    public Cupon(){

    }

    public Cupon(String restaurante, String informacion, String id){
        this.restaurante = restaurante;
        this.informacion = informacion;
        this.id = id;
    }

    public Cupon(String restaurante, String informacion){
        this.restaurante = restaurante;
        this.informacion = informacion;
    }
    public String getRestaurante(){return this.restaurante;}

    public String getInformacion(){return this.informacion;}

    public String getId(){return this.id;}
}
