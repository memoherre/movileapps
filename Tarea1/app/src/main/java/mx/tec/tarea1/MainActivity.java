package mx.tec.tarea1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SearchView;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private SearchView textBuscar;
    private Button bProfile, bRes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textBuscar = findViewById(R.id.textitoBuscar);
        this.bProfile = findViewById(R.id.bPro);
        this.bRes = findViewById(R.id.bRes);

        this.bProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Se ha entrado al perfil",Toast.LENGTH_SHORT).show();
            }
        });

        this.bRes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Se ha entrado a sus restaurantes",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
