package mx.tec.jan28;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPrefsActivity extends AppCompatActivity {

    private static final String PREFS_FILE = "Jan28";

    private SharedPreferences prefs;
    private EditText textito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefs);

        prefs = getSharedPreferences(PREFS_FILE,MODE_PRIVATE);

        textito = findViewById(R.id.valueText);


    }

    public void loadPrefs(View v){
        prefs = getSharedPreferences(PREFS_FILE,MODE_PRIVATE);
        Toast.makeText(this,"PREFS LOADED", Toast.LENGTH_SHORT).show();
    }
    public void savePrefs(View v){

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("llavecita",textito.getText().toString());
        // in order to save values to storage, commit
        editor.commit();
        Toast.makeText(this,"Value set", Toast.LENGTH_SHORT).show();
    }
    public void printValue(View v){

        Toast.makeText(this,prefs.getString("llavecita","No value"), Toast.LENGTH_SHORT).show();

    }
}
