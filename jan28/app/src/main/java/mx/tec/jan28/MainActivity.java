package mx.tec.jan28;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Button;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {

    // Local storage

    // Local database sqlite

    // Files

    // Prefs

    private EditText etID, name, grade;
    private DBHelper db;
    private Button bAdd, bFind, bDel;


    // Properties for Java Apps
    // A way to save key-value set on local storage

    private Properties properties;
    private static final String PROPERTIES_FILE = "properties.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DBHelper(this);

        this.name = findViewById(R.id.etName);
        this.grade = findViewById(R.id.etGrade);
        this.etID = findViewById(R.id.etID);

        this.etID.setHint("ID");
        this.name.setHint("Name");
        this.grade.setHint("Grade");

        this.bAdd = findViewById(R.id.bADD);
        this.bFind = findViewById(R.id.bF);
        this.bDel = findViewById(R.id.bD);

        properties = new Properties();

        File file =  new File(getFilesDir(), PROPERTIES_FILE);

        if(file.exists()){

            try {
                FileInputStream fis = openFileInput(PROPERTIES_FILE);
                properties.loadFromXML(fis);
                fis.close();
            }catch(InvalidPropertiesFormatException i){
                i.printStackTrace();
            }catch (FileNotFoundException g){
                g.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this,"CREATING FILE",Toast.LENGTH_SHORT).show();
            saveProperties();

        }
        /*
        Super User :

        Name : Cisco
        Grade : 1
         */
        this.db.save("Cisco",1);

    }

    private void saveProperties(){

        try {
            FileOutputStream fos = openFileOutput(PROPERTIES_FILE, Context.MODE_PRIVATE);
            properties.storeToXML(fos,null);
        }catch (FileNotFoundException f){
            f.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveToMemory(View v){
        properties.put("example","A value to save");
        Toast.makeText(this,"SAVING TO MEMORY...",Toast.LENGTH_SHORT).show();
    }
    public void saveToFile(View v){
        saveProperties();
        Toast.makeText(this,"SAVING TO FILE...",Toast.LENGTH_SHORT).show();
    }

    public void printPropert(View v){
        Toast.makeText(this,"SAVING TO FILE...",Toast.LENGTH_SHORT).show();
    }

    public void save(View v){
        db.save(name.getText().toString(),Integer.parseInt(grade.getText().toString()));
        Toast.makeText(this,"PROPERTY : "+properties.get("example"),Toast.LENGTH_SHORT);
    }

    public void find(View v){
        int result = db.find(name.getText().toString());
        grade.setText(result+"");
        Toast.makeText(this,"FINDING...",Toast.LENGTH_SHORT);
    }

    public void delete(View v){
        int rows = db.delete(Integer.parseInt(etID.getText().toString()));
        Toast.makeText(this,rows+"rows affected...",Toast.LENGTH_SHORT);
    }

    public void changeActivity(View v){
        Intent i = new Intent(this, SharedPrefsActivity.class);
    }

}
