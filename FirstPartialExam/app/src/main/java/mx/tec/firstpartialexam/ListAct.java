package mx.tec.firstpartialexam;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

public class ListAct extends AppCompatActivity implements AdapterView.OnItemClickListener ,AdapterView.OnItemSelectedListener{

    private ListView listita;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        spinner = findViewById(R.id.spinner);
        listita = findViewById(R.id.listV);

        String [] data = {"Pet 1", "Pet 2", "Pet 3"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                data
        );
        listita.setAdapter(adapter);
        listita.setOnItemClickListener(this);


        ArrayList<mx.tec.firstpartialexam.Pet> al = new ArrayList<>();

        al.add(new mx.tec.firstpartialexam.Pet("Solovino",1,20));
        al.add(new mx.tec.firstpartialexam.Pet("Firulais",2,10));
        al.add(new mx.tec.firstpartialexam.Pet("Scooby",10,40));

        CustomAdapter customAdapter = new CustomAdapter(al,this);

        listita.setAdapter(customAdapter);
        spinner.setAdapter(customAdapter);
        listita.setOnItemClickListener(this);
        spinner.setOnItemSelectedListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
