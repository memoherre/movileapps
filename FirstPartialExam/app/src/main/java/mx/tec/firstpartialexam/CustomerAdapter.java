package mx.tec.firstpartialexam;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CustomerAdapter extends BaseAdapter {

    private ArrayList<mx.tec.firstpartialexam.Pet> source;
    private Activity activity;

    public CustomerAdapter(ArrayList<mx.tec.firstpartialexam.Pet> source, Activity activity){
        this.source = source;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.source.size();
    }

    @Override
    public Object getItem(int position) {
        return this.source.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.source.get(position).getAge();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // The actual specific row
        if(convertView == null){
            //Inflate from xml to java object
            convertView = activity.getLayoutInflater().inflate(R.layout.row,null);
        }

        TextView name = convertView.findViewById(R.id.name);
        TextView grade = convertView.findViewById(R.id.grade);

        mx.tec.firstpartialexam.Pet thePet = source.get(position);
        name.setText(thePet.getName());
        grade.setText(thePet.getAge());

        return convertView;
    }
}

