package mx.tec.firstpartialexam;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {


    private TextView textito;
    private Button bList, bData;
    public Properties properties;
    private static final String PROPERTIES_FILE="properties.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textito = findViewById(R.id.textito);

        this.bList = findViewById(R.id.bList);
        this.bData = findViewById(R.id.bData);

        this.bList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListAct.class);
                startActivity(i);
            }
        });
        this.bData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, DataIn.class);
                startActivity(i);
            }
        });

        properties = new Properties();

        File file = new File(getFilesDir(), PROPERTIES_FILE);

        if(file.exists()){

            Toast.makeText(this, "LOADING FILE", Toast.LENGTH_SHORT).show();
            try {
                FileInputStream fis = openFileInput(PROPERTIES_FILE);
                properties.loadFromXML(fis);
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (InvalidPropertiesFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "CREATING FILE", Toast.LENGTH_SHORT).show();
            saveProperties();
        }

        properties.put("saludo", "Hola, este es un valor de properties");
        this.textito.setText((String)properties.get("saludo"));

    }
    private void saveProperties(){
        try {
            FileOutputStream fos = openFileOutput(PROPERTIES_FILE, Context.MODE_PRIVATE);
            properties.storeToXML(fos, null);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveToMemory(View v){
        properties.put("example", "A value to save");
        Toast.makeText(this, "SAVING TO MEMORY...", Toast.LENGTH_SHORT).show();
    }

    public void saveToFile(View v){
        saveProperties();
        Toast.makeText(this, "SAVING TO FILE...", Toast.LENGTH_SHORT).show();
    }

}
