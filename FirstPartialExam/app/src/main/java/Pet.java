package mx.tec.firstpartialexam;

public class Pet{


    private String name;
    private int age, weight;


    public Pet(String name, int age, int weight){

        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public int getAge() {
        return this.age;
    }

    public int getWeight() {
        return this.weight;
    }

    public String getName() {
        return this.name;
    }
}
