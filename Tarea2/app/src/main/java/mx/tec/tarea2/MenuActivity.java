package mx.tec.tarea2;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    private TextView textito1,
                     textito2;
    private String textMainA, texHob;

    final static int HOBBIES_CODE = 1234;
    final static int AMIGOS_CODE = 1244;
    final static int MSG_CODE = 12324;

    private ImageButton bHobbies, bAmigos, bMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        this.textito1 = findViewById(R.id.texMenuHola);
        this.textito2 = findViewById(R.id.textMenuEdit);
        this.bHobbies = findViewById(R.id.bHobbies);
        this.bAmigos = findViewById(R.id.bAmigos);
        this.bMsg = findViewById(R.id.bMsg);

        this.textMainA = getIntent().getExtras().getString("Value");

        this.textito1.setText("Hi "+this.textMainA);
        this.textito2.setText(" ");

        this.bHobbies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentitoH = new Intent(MenuActivity.this, Hobbies.class);
                startActivityForResult(intentitoH, HOBBIES_CODE);
              //  finish();
            }
        });

        this.bAmigos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentitoH = new Intent(MenuActivity.this, Friends.class);
                startActivityForResult(intentitoH, AMIGOS_CODE);
                //  finish();
            }
        });

        this.bMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentitoH = new Intent(MenuActivity.this, Message.class);
                startActivityForResult(intentitoH, MSG_CODE);
                //  finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == MenuActivity.HOBBIES_CODE) {
            if(resultCode == Activity.RESULT_OK){
                this.texHob = data.getStringExtra("Hobbies");
                this.textito2.setText(this.texHob);
            }
        }
        if(requestCode == MenuActivity.MSG_CODE) {
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this,"Message sent",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
