package mx.tec.tarea2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Friends extends AppCompatActivity {

    private Button botoncitoFriens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        this.botoncitoFriens = findViewById(R.id.bRetFrien);

        this.botoncitoFriens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentito = new Intent();
                setResult(Activity.RESULT_OK,intentito);
                finish();
            }
        });
    }

}
