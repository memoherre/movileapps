package mx.tec.tarea2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

public class Hobbies extends AppCompatActivity {

    private EditText eT;
    private Button botoncito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hobbies);

        this.eT = findViewById(R.id.textitoHob);

        this.botoncito = findViewById(R.id.bHobb);
        this.eT.setHint("What is your hobby?");


        this.botoncito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentito = new Intent();
                intentito.putExtra("Hobbies",Hobbies.this.eT.getText().toString());
                setResult(Activity.RESULT_OK,intentito);
                finish();
            }
        });

    }
}
