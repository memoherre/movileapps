package mx.tec.tarea2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import android.widget.Button;

public class Message extends AppCompatActivity {

    private Button botoncito;
    private EditText textito;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        this.botoncito = findViewById(R.id.botoncitoMSG);
        this.textito = findViewById(R.id.textMSG);

        this.textito.setHint("Escribe un mensaje");
        this.botoncito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentito = new Intent();
                intentito.putExtra("MSG",12324);
                setResult(Activity.RESULT_OK,intentito);
                finish();
            }
        });

    }
}
