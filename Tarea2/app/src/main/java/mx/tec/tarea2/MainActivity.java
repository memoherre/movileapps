package mx.tec.tarea2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText textito;
    private Button botoncito;
    private static final int VENTANA_MENU_CODE = 0;
    private String textPasar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textito =  findViewById(R.id.textito);
        this.botoncito = findViewById(R.id.botoncito);
        this.textito.setHint("Ingresa tu nombre");

        this.botoncito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentito = new Intent(MainActivity.this, MenuActivity.class);
                textPasar = textito.getText().toString();
                intentito.putExtra("Value", textPasar);
                startActivity(intentito);
                finish();
            }
        });
    }
}
