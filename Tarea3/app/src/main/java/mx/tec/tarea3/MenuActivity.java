package mx.tec.tarea3;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class MenuActivity extends AppCompatActivity {

    private EditText name, hobby;
    private TextView textito;
    private Button bAdd, bFind, bDel, bRet;
    private String tName, tHobby;
    private DBHelper db;
    private Properties properties;

    private static final String PROPERTIES_FILE = "archivito.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        this.name = findViewById(R.id.textitoNameMenu);
        this.hobby = findViewById(R.id.textitoHobbyMenu);
        this.textito = findViewById(R.id.textitoMenu);

        this.bAdd = findViewById(R.id.buttonAdd);
        this.bFind = findViewById(R.id.buttonFind);
        this.bDel = findViewById(R.id.buttonDel);
        this.bRet = findViewById(R.id.buttonRegresarMenu);

        this.db = new DBHelper(this);
        this.properties = new Properties();
        File file =  new File(getFilesDir(), PROPERTIES_FILE);

        if(file.exists()){

            try {
                FileInputStream fis = openFileInput(PROPERTIES_FILE);
                properties.loadFromXML(fis);
                fis.close();
            }catch(InvalidPropertiesFormatException i){
                i.printStackTrace();
            }catch (FileNotFoundException g){
                g.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this,"CREATING FILE",Toast.LENGTH_SHORT).show();
            saveProperties();
        }

        this.tName = getIntent().getExtras().getString("Nombre");
        this.tHobby = getIntent().getExtras().getString("Hobby");

        this.name.setText(tName);
        this.hobby.setText(tHobby);

        this.bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity.this.find(v);
            }
        });

        this.bRet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity.this.changeActivity(v);
            }
        });
        this.bFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity.this.find(v);
            }
        });
        this.bDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity.this.delete(v);
            }
        });
    }

    private void saveProperties(){

        try {
            FileOutputStream fos = openFileOutput(PROPERTIES_FILE, Context.MODE_PRIVATE);
            properties.storeToXML(fos,null);
        }catch (FileNotFoundException f){
            f.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(View v){
        db.save(name.getText().toString(),hobby.getText().toString());
        properties.put(name.getText().toString(),hobby.getText().toString());
        saveProperties();
        Toast.makeText(this,"Value saved ",Toast.LENGTH_SHORT);
    }

    public void find(View v){
        int result = db.find(name.getText().toString());
        hobby.setText(result+"");
        Toast.makeText(this,"Value found",Toast.LENGTH_SHORT);
    }

    public void delete(View v){
        int rows = db.delete(name.getText().toString());
        Toast.makeText(this,rows+"rows affected...",Toast.LENGTH_SHORT);
    }

    public void changeActivity(View v){
        Intent i = new Intent(this, MainActivity.class);
    }

}
