package mx.tec.tarea3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private EditText textName, textHobby;
    private Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textName = findViewById(R.id.textitoName);
        this.textHobby = findViewById(R.id.textitoHobby);
        this.boton = findViewById(R.id.botoncito);

        //Boton on-click logic is in the xml file.
        this.boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentito = new Intent(MainActivity.this, MenuActivity.class);

                String textPasar = textName.getText().toString();
                if(textPasar.equals("") || textPasar.equals(" ")){
                    textPasar = "Memo-default";
                }
                intentito.putExtra("Nombre", textPasar);

                textPasar = textHobby.getText().toString();
                if(textPasar.equals("") || textPasar.equals(" ")){
                    textPasar = "Correr-default";
                }
                intentito.putExtra("Hobby", textPasar);

                startActivity(intentito);
                finish();
            }
        });

    }
}
