package mx.tec.jan21;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Button;
import android.view.*;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView textito;
    private EditText input;
    private SeekBar barrita;
    private Button botoncito1,
                   botoncito2;
    private static final int PERRITO_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Retrieving references to widgets
        textito = findViewById(R.id.textView);
        input = findViewById(R.id.editText);
        barrita =  findViewById(R.id.seekBar);
        botoncito1 = findViewById(R.id.button);
        botoncito2 = findViewById(R.id.button2);

        textito.setText("This is from Code");
        input.setHint("Hey this is an excelent Hint!");

        botoncito2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"The other button",Toast.LENGTH_SHORT).show();
            }
        });
    }

    // listen for a button pressed
    // 2 main choices

    //  - link through XML
    // - add listener through code

    // Link through XML Example ; Method signature with return void and receives a view

    public void buttonTest(View v){

        // how to log
        Log.e("Main", "Fatal Error that is terrible");

        //Do the toast
        // factory method from the Toast class
        Toast.makeText(this, input.getText().toString(), Toast.LENGTH_SHORT).show();
 //     Toast.makeText(getApplicationContext(), "Hey Guys! ", Toast.LENGTH_SHORT).show();
        // intent - request from to open an activity
        // we dont order an activity to be open, we request.

        Intent intent = new Intent(this, PerritoActivity.class);
        intent.putExtra("userName", input.getText().toString());
        intent.putExtra("Age", 34);
        //startActivity(intent);
        startActivityForResult(intent,MainActivity.PERRITO_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == MainActivity.PERRITO_CODE){
            if(resultCode == Activity.RESULT_OK){
                Toast.makeText(this,data.getStringExtra("Return value"),Toast.LENGTH_SHORT).show();
            }
        }

    }
}
